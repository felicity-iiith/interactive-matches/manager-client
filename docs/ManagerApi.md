# manager_client.ManagerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**evaluate**](ManagerApi.md#evaluate) | **POST** /action | Evaluate the action given by the player
[**get_state**](ManagerApi.md#get_state) | **GET** /state | Returns current game state
[**init**](ManagerApi.md#init) | **POST** /init | Start the whole process
[**invalidate**](ManagerApi.md#invalidate) | **POST** /invalid | Invalidate a player
[**quit**](ManagerApi.md#quit) | **POST** /quit | Quit this server


# **evaluate**
> object evaluate(inline_object1)

Evaluate the action given by the player

The player (probably via some middleman) sent an action for the current state. This evaluates the action

### Example

```python
from __future__ import print_function
import time
import manager_client
from manager_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = manager_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with manager_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = manager_client.ManagerApi(api_client)
    inline_object1 = manager_client.InlineObject1() # InlineObject1 | 

    try:
        # Evaluate the action given by the player
        api_response = api_instance.evaluate(inline_object1)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ManagerApi->evaluate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object1** | [**InlineObject1**](InlineObject1.md)|  | 

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successfully processed the given action |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_state**
> InlineResponse2001 get_state()

Returns current game state

Returns the game state, as maintained by the manager. May be used for forwarding it to the players or for viewing the game on a website

### Example

```python
from __future__ import print_function
import time
import manager_client
from manager_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = manager_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with manager_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = manager_client.ManagerApi(api_client)
    
    try:
        # Returns current game state
        api_response = api_instance.get_state()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ManagerApi->get_state: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Current game state |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **init**
> InlineResponse200 init(inline_object)

Start the whole process

The manager object is created. It gets to know about the players who are playing.

### Example

```python
from __future__ import print_function
import time
import manager_client
from manager_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = manager_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with manager_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = manager_client.ManagerApi(api_client)
    inline_object = manager_client.InlineObject() # InlineObject | 

    try:
        # Start the whole process
        api_response = api_instance.init(inline_object)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ManagerApi->init: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object** | [**InlineObject**](InlineObject.md)|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successfully extracted action from code |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **invalidate**
> InlineResponse200 invalidate(inline_object2)

Invalidate a player

A player either made an illegal move OR the request timed out. The player is disqualified and can't play anymore. Remove the player from active list

### Example

```python
from __future__ import print_function
import time
import manager_client
from manager_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = manager_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with manager_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = manager_client.ManagerApi(api_client)
    inline_object2 = manager_client.InlineObject2() # InlineObject2 | 

    try:
        # Invalidate a player
        api_response = api_instance.invalidate(inline_object2)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ManagerApi->invalidate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object2** | [**InlineObject2**](InlineObject2.md)|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successfully removed the player from active list |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **quit**
> str quit()

Quit this server

When the game is over and this manager is no longer required. Shutting down server to reduce resource usage

### Example

```python
from __future__ import print_function
import time
import manager_client
from manager_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = manager_client.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with manager_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = manager_client.ManagerApi(api_client)
    
    try:
        # Quit this server
        api_response = api_instance.quit()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling ManagerApi->quit: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Starts the process of shutting down |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

